import { getType } from 'typesafe-actions';

import { AllResourcePackagesFetchStatus, ResourcesPackage, ResourcePackagesState } from '../../util/types';
import * as actions from '../actions';
import { RootAction } from '../reducers';

const initialWholesales: ResourcePackagesState = {
    resourcePackageSelected: null,
    allResourcePackages: {},
    allResourcePackagesForSale: {},
    allResourcePackagesFetchStatus: AllResourcePackagesFetchStatus.Request,
};

export function wholesales(state: ResourcePackagesState = initialWholesales, action: RootAction): ResourcePackagesState {
    // console.log('wholesale reducers action = ' + action.type);
    switch (action.type) {
        case getType(actions.fetchAllWholesalesAsync.success):
            const allResourcePackages: { [key: string]: ResourcesPackage } = {};
            console.log(action.payload.resourcesPackages);
            action.payload.resourcesPackages.forEach(resourcePackage => {
                allResourcePackages[resourcePackage.tokenId] = resourcePackage;
            });
            const allResourcePackagesForSale: { [key: string]: ResourcesPackage } = {};
            action.payload.resourcesPackagesForSale.forEach(resourcePackage => {
                allResourcePackagesForSale[resourcePackage.tokenId] = resourcePackage;
            });
            const allResourcePackagesFetchStatus = AllResourcePackagesFetchStatus.Success;
            return { ...state, allResourcePackages, allResourcePackagesForSale, allResourcePackagesFetchStatus };
        case getType(actions.selectWholesales):
            return { ...state, resourcePackageSelected: action.payload };
        default:
            return state;
    }
}
