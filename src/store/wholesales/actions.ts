import { MetamaskSubprovider, signatureUtils, SignedOrder } from '0x.js';
import { createAction, createAsyncAction } from 'typesafe-actions';

import { ZERO_ADDRESS } from '../../common/constants';
import { cancelSignedOrder } from '../../services/orders';
import { getLogger } from '../../util/logger';
import { isDutchAuction } from '../../util/orders';
import { getTransactionOptions } from '../../util/transactions';
import { ResourcesPackage, ThunkCreator, Collectible } from '../../util/types';
import { getEthAccount, getGasPriceInWei } from '../selectors';

const logger = getLogger('Wholesales::Actions');

export const fetchAllWholesalesAsync = createAsyncAction(
    'wholesales/ALL_WHOLESALES_fetch_request',
    'wholesales/ALL_WHOLESALES_fetch_success',
    'wholesales/ALL_WHOLESALES_fetch_failure',
)<
    void,
    {
        resourcesPackages: ResourcesPackage[];
        resourcesPackagesForSale: ResourcesPackage[];
    },
    Error
>();

export const selectWholesales = createAction('wholesales/selectWholesale', resolve => {
    return (resourcesPackages: ResourcesPackage | null) => resolve(resourcesPackages);
});

export const getAllWholesales: ThunkCreator = () => {
    console.log("getAllWholesales");
    return async (dispatch, getState, { getWholesalesMetadataGateway, getWeb3Wrapper }) => {
        dispatch(fetchAllWholesalesAsync.request());
        try {
            const state = getState();
            const ethAccount = getEthAccount(state);
            const resourcesPackagesMetadataGateway = getWholesalesMetadataGateway();
            const resourcesPackages = await resourcesPackagesMetadataGateway.fetchAllWholesales(ethAccount);
            const resourcesPackagesForSale = await resourcesPackagesMetadataGateway.fetchAllWholesalesForSale();
            dispatch(fetchAllWholesalesAsync.success({ resourcesPackages, resourcesPackagesForSale }));
        } catch (err) {
            logger.error('There was a problem fetching the wholesales', err);
            dispatch(fetchAllWholesalesAsync.failure(err));
        }
    };
};

export const submitReserveResourcesPackage: ThunkCreator<Promise<string>> = (collectible: Collectible, ethAccount: string) => {
    return async (dispatch, getState, { getContractWrappers, getWeb3Wrapper }) => {
        console.log("submitReserveResourcesPackage");
        if(!(collectible as ResourcesPackage)){
            console.log("collectible not ResourcesPackage");
            return;
        }
        const contractWrappers = await getContractWrappers();
        const web3Wrapper = await getWeb3Wrapper();

        const state = getState();
        const gasPrice = getGasPriceInWei(state);
        const gasOptions = getTransactionOptions(gasPrice);

        let tx = await contractWrappers.reserveResourcesPackage(<ResourcesPackage>collectible);
        
        await web3Wrapper.awaitTransactionSuccessAsync(tx);

        // tslint:disable-next-line:no-floating-promises
        dispatch(getAllWholesales());
        return tx;
    };
};

export const cancelOrderResourcesPackage: ThunkCreator = (order: any) => {
    return async (dispatch, getState) => {
        const state = getState();
        const gasPrice = getGasPriceInWei(state);

        return cancelSignedOrder(order, gasPrice).then(transaction => {
            // tslint:disable-next-line:no-floating-promises
            dispatch(getAllWholesales());
        });
    };
};
