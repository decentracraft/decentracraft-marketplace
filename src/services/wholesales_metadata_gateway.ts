import { assetDataUtils, SignedOrder } from '0x.js';

import { COLLECTIBLE_ADDRESS } from '../common/constants';
import { getRelayer, Relayer } from '../services/relayer';
import { getKnownTokens } from '../util/known_tokens';
import { getLogger } from '../util/logger';
import { ResourcesPackage, ResourcesPackagesMetadataSource } from '../util/types';

import { getConfiguredSource } from './wholesale_metadata_sources';

const logger = getLogger('WholesalesMetadataGateway');

export class WholesalesMetadataGateway {
    private readonly _relayer: Relayer;
    private readonly _source: ResourcesPackagesMetadataSource;

    private collectiblesChunkFetched: any;

    constructor(relayer: Relayer, source: ResourcesPackagesMetadataSource) {
        this._relayer = relayer;
        this._source = source;
    }

    public fetchAllWholesales = async (userAddress?: string): Promise<ResourcesPackage[]> => {
        console.log("fetchAllWholesales");
        console.log("userAddress = " + userAddress );
        // const knownTokens = getKnownTokens();

        // const wethAddress = knownTokens.getWethToken().address;

        // console.log("COLLECTIBLE_ADDRESS = " + COLLECTIBLE_ADDRESS );
        // console.log("wethAddress = " + wethAddress );
        // // Step 1: Get all sell orders in the relayer
        // let orders: any[] = [];
        // try {
        //     orders = await this._relayer.getSellCollectibleOrdersAsync(COLLECTIBLE_ADDRESS, wethAddress);
        // } catch (err) {
        //     logger.error(err);
        //     throw err;
        // }

        // const tokenIdToOrder = orders.reduce<{ [tokenId: string]: SignedOrder }>((acc, order) => {
        //     const { tokenId } = assetDataUtils.decodeERC721AssetData(order.makerAssetData);
        //     acc[tokenId.toString()] = order;
        //     return acc;
        // }, {});

        // // Step 2: Get all the user's collectibles and add the order
        let collectiblesWithOrders: ResourcesPackage[] = [];
        console.log("userAddress = " + userAddress );
        if (userAddress) {
            const userCollectibles = await this._source.fetchAllUserResourcesPackagesAsync(userAddress);
            collectiblesWithOrders = userCollectibles.map(collectible => {
                // if (tokenIdToOrder[collectible.tokenId]) {
                //     return {
                //         ...collectible,
                //         order: tokenIdToOrder[collectible.tokenId],
                //     };
                // }

                return collectible;
            });
        }

        // // Step 3: Get collectibles that are not from the user
        // let collectiblesFetched: any[] = [];
        // const tokenIds: string[] = Object.keys(tokenIdToOrder).filter(
        //     tokenId => !collectiblesWithOrders.find(collectible => collectible.tokenId === tokenId),
        // );
        // for (let chunkBegin = 0; chunkBegin < tokenIds.length; chunkBegin += 10) {
        //     const tokensIdsChunk = tokenIds.slice(chunkBegin, chunkBegin + 10);
        //     const collectiblesChunkFetched = await this._source.fetchAllResourcesPackagesAsync(/* tokensIdsChunk */);
        //     const collectiblesChunkWithOrders = collectiblesChunkFetched.map(collectible => ({
        //         ...collectible,
        //         order: tokenIdToOrder[collectible.tokenId],
        //     }));
        //     collectiblesFetched = collectiblesFetched.concat(collectiblesChunkWithOrders);
        // }

        // collectiblesWithOrders.push(...collectiblesFetched);

        // return collectiblesWithOrders;

        console.log('source = ' + this._source);
        //this.collectiblesChunkFetched = await this._source.fetchAllResourcesPackagesAsync(/* tokensIdsChunk */);
        
        //this.collectiblesChunkFetched.push(...collectiblesWithOrders);

        return collectiblesWithOrders;
    };

    
    public fetchAllWholesalesForSale = async (): Promise<ResourcesPackage[]> => {
        console.log("fetchAllWholesalesForSale");
        let resourcesPackagesForSale: ResourcesPackage[] = [];

        // Get wholesales that are for sale
        // let collectiblesFetched: any[] = [];
        // const tokenIds: string[] = Object.keys(tokenIdToOrder).filter(
        //     tokenId => !collectiblesWithOrders.find(collectible => collectible.tokenId === tokenId),
        // );
        // for (let chunkBegin = 0; chunkBegin < tokenIds.length; chunkBegin += 10) {
        //     const tokensIdsChunk = tokenIds.slice(chunkBegin, chunkBegin + 10);
        //     const collectiblesChunkFetched = await this._source.fetchAllResourcesPackagesAsync(/* tokensIdsChunk */);
        //     const collectiblesChunkWithOrders = collectiblesChunkFetched.map(collectible => ({
        //         ...collectible,
        //         order: tokenIdToOrder[collectible.tokenId],
        //     }));
        //     collectiblesFetched = collectiblesFetched.concat(collectiblesChunkWithOrders);
        // }

        // collectiblesWithOrders.push(...collectiblesFetched);

        const collectiblesChunkFetched = await this._source.fetchAllResourcesPackagesAsync(/* tokensIdsChunk */);
        return collectiblesChunkFetched;
    }
}

let wholesalesMetadataGateway: WholesalesMetadataGateway;
export const getWholesalesMetadataGateway = (): WholesalesMetadataGateway => {
    if (!wholesalesMetadataGateway) {
        const relayer = getRelayer();
        const source = getConfiguredSource();
        wholesalesMetadataGateway = new WholesalesMetadataGateway(relayer, source);
    }
    return wholesalesMetadataGateway;
};
