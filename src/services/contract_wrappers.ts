import { ContractWrappers } from '0x.js';
import { SupportedProvider } from '0x.js';
import { ContractWrappersConfig } from '0x.js';

import { BigNumber } from '0x.js';

import { ResourcesPackage } from '../util/types';
import { NETWORK_ID } from '../common/constants';

import { getWeb3Wrapper } from './web3_wrapper';
import { Web3Wrapper } from '@0x/web3-wrapper';

import { TxData } from 'ethereum-types';

import DecentracraftWorld from '../contracts/DecentracraftWorld.json';

import Web3 from 'web3'; 

// const Contract = require('truffle-contract');
// const fs = require('fs');

// const buildPath = __dirname + "/../build/contracts/";
// console.log("buildPath = " + buildPath);

// const buildFiles = fs.readdirSync(buildPath);
// console.log("buildFiles = " + buildFiles + " size = " + buildFiles.length);

// var abis = [];

let web3Wrapper: Web3Wrapper;
let contractWrappers: DCCContractWrappers;

//@ts-ignore
var dccWorld;
//@ts-ignore
var web3;

class DCCContractWrappers extends ContractWrappers{

    constructor(supportedProvider: SupportedProvider, config: ContractWrappersConfig){
        super(supportedProvider, config);
        
        web3 = new Web3(window.ethereum);
        // console.log(web3.eth);
        console.log(web3);
        console.log(DecentracraftWorld.abi);
        //@ts-ignore
        const deployedNetwork = DecentracraftWorld.networks[NETWORK_ID];
        //@ts-ignore
        dccWorld = new web3.eth.Contract(DecentracraftWorld.abi,
            deployedNetwork && deployedNetwork.address,);
        console.log(dccWorld);
        console.log("Finished creating dccworld contract");

        // this.loadContracts();
    }

    async reserveResourcesPackage(collectible : ResourcesPackage){
        console.log("reserveResourcesPackage");
        //@ts-ignore
        var price = new BigNumber(web3.utils.toWei(collectible.price.toString(),"wei"));
        console.log("price = " + price);
        //@ts-ignore
        var tokenid = Number(collectible.tokenId);
        console.log("tokenid = " + tokenid);
        //@ts-ignore
        var selectedaddress = await web3.givenProvider.selectedAddress;
        console.log("selectedaddress = " + selectedaddress);
        //@ts-ignore
        var tx = await dccWorld.methods.reserveResources(tokenid).send({from: selectedaddress, value: price});
        console.log("tx = " + tx.transactionHash);
        return tx.transactionHash;
    }
}

export const getContractWrappers = async () => {
    if (!contractWrappers) {
        web3Wrapper = await getWeb3Wrapper();
        contractWrappers = new DCCContractWrappers(web3Wrapper.getProvider(), { networkId: NETWORK_ID });
    }

    return contractWrappers;
};
