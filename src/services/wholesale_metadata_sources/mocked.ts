import { BigNumber } from '0x.js';

import { ResourcesPackage, ResourcesPackagesMetadataSource } from '../../util/types';

const allPackages: ResourcesPackage[] = [
    {
        tokenId: '0',
        name: 'Basica',
        price: new BigNumber(100000000000000000),
        order: null,
        color: '#F6FEFC',
        image: 'https://res.cloudinary.com/ddklsa6jc/image/upload/v1556888670/6_w93q19.png',
        assetUrl: 'https://www.cryptokitties.co/',
        description: '',
        currentOwner: '0x600597cdAeF3F0a22553d0664bEe9cD543FD6f1B',
        resources: [{
            id: '340282366920938463463374607431768211456',
            uri: 'https://dccapi2.now.sh/public/340282366920938463463374607431768211456.json',
            supply: 300
        },
        {
            id: '680564733841876926926749214863536422912',
            uri: 'https://dccapi2.now.sh/public/680564733841876926926749214863536422912.json',
            supply: 200            
        }],
        nfts: [{
            id: '10',
            uri: 'https://dccapi2.now.sh/public/hammer.json',
            json: '{\"attack\": \"6\",\"defense\": \"1\",\"speed\": \"1\"}',
            probability: 4,
            supply: 10
        },
        {
            id: '11',
            uri: 'https://dccapi2.now.sh/public/hammer.json',
            json: '{\"attack\": \"8\",\"defense\": \"2\",\"speed\": \"1\"}',
            probability: 1  ,
            supply: 7          
        }],
    },
    {
        tokenId: '1',
        name: 'BasicaLargia',
        price: new BigNumber(170000000000000000),
        order: null,
        color: '#F6C68A',
        image: 'https://res.cloudinary.com/ddklsa6jc/image/upload/v1556888668/9_xunbhn.png',
        assetUrl: 'https://www.cryptokitties.co/',
        description: '',
        currentOwner: '',
        resources: [{
            id: '1',
            uri: 'https://dccapi2.now.sh/public/340282366920938463463374607431768211456.json',
            supply: 1000
        },
        {
            id: '2',
            uri: 'https://dccapi2.now.sh/public/340282366920938463463374607431768211456.json',
            supply: 500            
        },],
        nfts: [{
            id: '10',
            uri: 'https://dccapi2.now.sh/public/hammer.json',
            json: '{\"attack\": \"6\",\"defense\": \"1\",\"speed\": \"1\"}',
            probability: 6,
            supply: 9
        },
        {
            id: '11',
            uri: 'https://dccapi2.now.sh/public/hammer.json',
            json: '{\"attack\": \"6\",\"defense\": \"1\",\"speed\": \"1\"}',
            probability: 2 ,
            supply: 6           
        },],
    },
];

export class ResourcesPackagesMocked implements ResourcesPackagesMetadataSource {
    public fetchAllUserResourcesPackagesAsync = async (userAddress: string): Promise<ResourcesPackage[]> => {
        console.log("fetchAllUserResourcesPackagesAsync");
        // const contractAddress = COLLECTIBLE_ADDRESS;
        // const contractWrappers = await getContractWrappers();

        // const allCollectiblesWithOwner = await Promise.all(
        //     allPackages.map(async collectible => {
        //         const owner = await contractWrappers.erc721Token.getOwnerOfAsync(
        //             contractAddress,
        //             new BigNumber(collectible.tokenId),
        //         );

        //         console.log("owner = " + owner);
        //         return {
        //             ...collectible,
        //             currentOwner: owner,
        //         };
        //     }),
        // );

        return allPackages;
    };

    public fetchAllResourcesPackagesAsync = async (): Promise<ResourcesPackage[]> => {
        console.log("fetchAllResourcesPackagesAsync");
        // const collectibles = allPackages.filter(value => tokenIds.indexOf(value.tokenId) !== -1);
        // return Promise.resolve(collectibles);
        return allPackages;
    };
}
