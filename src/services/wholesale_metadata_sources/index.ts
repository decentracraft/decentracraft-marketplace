import { WHOLESALES_SOURCE } from '../../common/constants';
import { ResourcesPackagesMetadataSource } from '../../util/types';

import { ResourcesPackagesMocked } from './mocked';
import { DCCApi } from './dcc';

const sources: { [key: string]: ResourcesPackagesMetadataSource } = {
    dcc: new DCCApi({ rps: 1 }),
    mocked: new ResourcesPackagesMocked(),
};

export const getConfiguredSource = () => {
    return sources[WHOLESALES_SOURCE.toLowerCase()];
};
