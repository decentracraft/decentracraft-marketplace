import { BigNumber } from '0x.js';
import { RateLimit } from 'async-sema';

import { NETWORK_ID } from '../../common/constants';
import { ResourcesPackage, ResourcesPackagesMetadataSource } from '../../util/types';

export class DCCApi implements ResourcesPackagesMetadataSource {
    private readonly _rateLimit: () => Promise<void>;

    private readonly _endpointsUrls: { [key: number]: string } = {
        1: 'https://api.opensea.io/api/v1',
        3: 'https://dccapi2.now.sh/api/',
        4: 'https://rinkeby-api.opensea.io/api/v1',
        50: 'http://192.168.1.110:3010/api/',
    };

    public static getAssetsAsResourcesPackages(assets: any[]): ResourcesPackage[] {
        console.log(assets);
        return Array.from(assets).map((asset: any) => {
            return DCCApi.getAssetAsResourcesPackage(asset);
        });
    }

    public static getAssetAsResourcesPackage(asset: any): ResourcesPackage {
        console.log(asset);
        return {
            tokenId: asset.packageID,
            name: asset.name || `${asset.asset_contract.name} - #${asset.token_id}`,
            color: asset.color ? `#${asset.background_color}` : '',
            image: asset.image,
            currentOwner: asset.currentOwner,
            assetUrl: asset.assetUrl,
            description: asset.description,
            order: null,
            price: new BigNumber(asset.price),
            resources: asset.resources,
            nfts: asset.nfts,
        };
    }

    constructor(options: { rps: number }) {
        this._rateLimit = RateLimit(options.rps); // requests per second
    }

    private readonly _fetch = async (url: string, userAddress: string) => {
        // let response: Response;
        let response = new Response();
        try{
            await this._rateLimit();
            let bodytxt = {
            player: userAddress
            };
            let body = JSON.stringify(bodytxt);
            console.log(body);
            response = await fetch(url, {
                method: "POST",
                headers: { 
                // "Access-Control-Allow-Origin": "*",
                // "Access-Control-Allow-Headers": "access-control-allow-headers, Access-Control-Allow-Origin, Content-Type",
                // "Content-Type": "application/json" 
                // 'Accept': 'application/json',
                // 'Content-Type': 'application/json',
                } as any,
                body: body
            });
        } catch (e) {
            console.log("failed to fetch dcc")
            console.log(e.toString())
        }
        console.log("Returning from fetch");
        return response;
    };

    public fetchAllUserResourcesPackagesAsync = async (userAddress: string): Promise<ResourcesPackage[]> => {
        console.log("dcc fetchAllUserResourcesPackagesAsync");
        console.log(userAddress);
        
        const metadataSourceUrl = this._endpointsUrls[NETWORK_ID];
        const url = `${metadataSourceUrl}userResourcesPackages`;
        // const url = "https://ab309815.ngrok.io/api/userResourcesPackages";
        console.log(url);
        const assetsResponse = await this._fetch(url, userAddress);
        const assetsResponseJson = await assetsResponse.json();
        const assets = DCCApi.getAssetsAsResourcesPackages(assetsResponseJson);
        console.log(assets);
        return assets;

        // return allPackages;
    };

    public fetchAllResourcesPackagesAsync = async (): Promise<ResourcesPackage[]> => {
        console.log("dcc fetchAllResourcesPackagesAsync");
        
        const metadataSourceUrl = this._endpointsUrls[NETWORK_ID];
        const url = `${metadataSourceUrl}resourcesPackages`;
        console.log(url);
        const assetsResponse = await this._fetch(url, "");
        const assetsResponseJson = await assetsResponse.json();
        const assets = DCCApi.getAssetsAsResourcesPackages(assetsResponseJson);
        console.log(assets);
        return assets;
    };
}
