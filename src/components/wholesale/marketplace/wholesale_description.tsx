import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';

import { COLLECTIBLE_NAME } from '../../../common/constants';
import { getWholesaleById, getEthAccount } from '../../../store/selectors';
import { truncateAddress } from '../../../util/number_utils';
import { ResourcesPackage, StoreState } from '../../../util/types';
import { Card } from '../../common/card';
import { OutsideUrlIcon } from '../../common/icons/outside_url_icon';

import { DutchAuctionPriceChartCard } from './dutch_auction_price_chart_card';
import { ResourceCardContainer } from '../collectibles/resource_card';
import { NFTCardContainer } from '../collectibles/nft_card';

const CollectibleDescriptionWrapper = styled.div``;

const CollectibleDescriptionTitleWrapper = styled.div`
    align-items: center;
    display: flex;
    justify-content: space-between;
    margin: 0 0 20px;
`;

const CollectibleDescriptionTitle = styled.h3`
    color: ${props => props.theme.componentsTheme.cardTitleColor};
    font-size: 18px;
    font-weight: 600;
    line-height: 1.2;
    margin: 0;
    padding: 0 15px 0 0;
`;

const CollectibleDescriptionType = styled.a`
    align-items: center;
    display: flex;
    text-decoration: none;
`;

const CollectibleDescriptionTypeText = styled.span`
    color: ${props => props.theme.componentsTheme.cardTitleColor};
    font-size: 14px;
    font-weight: 500;
    line-height: 1.2;
    margin: 0 6px;
`;

const CollectibleDescriptionTypeImage = styled.span<{ backgroundImage: string }>`
    background-image: url('${props => props.backgroundImage}');
    background-position: 50% 50%;
    background-repeat: no-repeat;
    background-size: cover;
    border-radius: 50%;
    height: 16px;
    width: 16px;
`;

export const CollectibleDescriptionInnerTitle = styled.h4`
    color: ${props => props.theme.componentsTheme.cardTitleColor};
    font-size: 16px;
    font-weight: 500;
    line-height: 1.2;
    margin: 0 5 10px;
`;

const CollectibleDescriptionText = styled.p`
    color: ${props => props.theme.componentsTheme.cardTitleColor};
    font-feature-settings: 'calt' 0;
    font-size: 14px;
    line-height: 1.6;
    margin: 0 0 20px;

    &:last-child {
        margin: 0;
    }
`;

const CollectibleOwnerWrapper = styled.div`
    align-items: center;
    display: flex;
`;

const CollectibleOwnerImage = styled.span<{ backgroundImage: string }>`
    background-image: url('${props => props.backgroundImage}');
    background-position: 50% 50%;
    background-repeat: no-repeat;
    background-size: cover;
    border-radius: 50%;
    height: 20px;
    margin: 0 8px 0 0;
    width: 20px;
`;

const CollectibleOwnerText = styled.p`
    color: ${props => props.theme.componentsTheme.cardTitleColor};
    font-feature-settings: 'calt' 0;
    font-size: 14px;
    line-height: 1.2;
    margin: 0;
`;

const CollectibleCard = styled(Card)`
    > div {
        min-height: 0;
    }
`;

interface OwnProps {
    collectibleId: string;
    type: string;
}

interface StateProps {
    collectible: ResourcesPackage | undefined;
    ethAccount: string;
}

type Props = OwnProps & StateProps;

const CollectibleDescription = (props: Props) => {
    console.log('CollectibleDescription');
    const { collectible, ethAccount, ...restProps } = props;

    if (!collectible) {
        return null;
    }

    const { tokenId, currentOwner, description, name, assetUrl } = collectible;
    const typeImage = 'https://placeimg.com/32/32/any';
    const ownerImage = 'https://placeimg.com/50/50/any';

    const doesBelongToCurrentUser = currentOwner.toLowerCase() === ethAccount.toLowerCase();

    return (
        <CollectibleDescriptionWrapper {...restProps}>
            <CollectibleCard>
                <CollectibleDescriptionTitleWrapper>
                    <CollectibleDescriptionTitle>{name}</CollectibleDescriptionTitle>
                    <CollectibleDescriptionType href={assetUrl} target="_blank">
                        <CollectibleDescriptionTypeImage backgroundImage={typeImage} />
                        <CollectibleDescriptionTypeText>{COLLECTIBLE_NAME}</CollectibleDescriptionTypeText>
                        {OutsideUrlIcon()}
                    </CollectibleDescriptionType>
                </CollectibleDescriptionTitleWrapper>
                <CollectibleDescriptionInnerTitle>ID: {tokenId}</CollectibleDescriptionInnerTitle>
                {description ? (
                    <>
                        <CollectibleDescriptionInnerTitle>Description</CollectibleDescriptionInnerTitle>
                        <CollectibleDescriptionText>{description}</CollectibleDescriptionText>
                    </>
                ) : null}
                {currentOwner ? (
                    <>
                        <CollectibleDescriptionInnerTitle>Current owner</CollectibleDescriptionInnerTitle>
                        <CollectibleOwnerWrapper>
                            <CollectibleOwnerImage backgroundImage={ownerImage} />
                            <CollectibleOwnerText>
                                {truncateAddress(currentOwner)}
                                {doesBelongToCurrentUser && ' (you)'}
                            </CollectibleOwnerText>
                        </CollectibleOwnerWrapper>
                    </>
                ) : null}
                <CollectibleDescriptionInnerTitle>Resources</CollectibleDescriptionInnerTitle>
                {
                    collectible.resources.map(function(resource){                    
                        return (
                            <ResourceCardContainer key={resource.id} resource={resource} ></ResourceCardContainer>
                        );
                    })
                }
                <CollectibleDescriptionInnerTitle>Collectibles</CollectibleDescriptionInnerTitle>
                {
                    collectible.nfts.map(function(nft){                    
                        return (
                            <NFTCardContainer key={nft.id} nft={nft}></NFTCardContainer>
                        );
                    })
                }
            </CollectibleCard>
            <DutchAuctionPriceChartCard collectible={collectible} />
        </CollectibleDescriptionWrapper>
    );
};

const mapStateToProps = (state: StoreState, props: OwnProps): StateProps => {
    return {
        collectible: getWholesaleById(state, props),
        ethAccount: getEthAccount(state),
    };
};

const CollectibleDescriptionContainer = connect(mapStateToProps)(CollectibleDescription);

export { CollectibleDescription, CollectibleDescriptionContainer };
