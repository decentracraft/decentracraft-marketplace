import React from 'react';

import { COLLECTIBLE_NAME } from '../../../common/constants';
import { AllWholesalesListContainer } from '../collectibles/wholesale_list';
import { Content } from '../common/content_wrapper';

export const ListCollectibles = () => (
    <Content>
        <AllWholesalesListContainer title={COLLECTIBLE_NAME} />
    </Content>
);
