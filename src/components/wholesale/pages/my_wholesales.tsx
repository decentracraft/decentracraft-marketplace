import React from 'react';

import { MyWholesalesListContainer } from '../collectibles/wholesale_list';
import { Content } from '../common/content_wrapper';

export const MyCollectibles = () => (
    <Content>
        <MyWholesalesListContainer title="My Packages" />
    </Content>
);
