import React from 'react';
import { connect } from 'react-redux';
import styled, { withTheme } from 'styled-components';

import { ReactComponent as LogoSvg } from '../../../assets/icons/erc721_logo.svg';
import { goToHome, goToWholesale } from '../../../store/router/actions';
import { Theme, themeBreakPoints } from '../../../themes/commons';
import { Logo } from '../../common/logo';
import { separatorTopbar, ToolbarContainer } from '../../common/toolbar';
import { NotificationsDropdownContainer } from '../../notifications/notifications_dropdown';
import { WalletConnectionContentContainer } from '../account/wallet_connection_content';
import { CollectiblesSearch } from '../collectibles/wholesale_search';

interface DispatchProps {
    onGoToHome: () => any;
    goToWholesale: () => any;
}

interface OwnProps {
    theme: Theme;
}

type Props = DispatchProps & OwnProps;

const MyWalletLink = styled.a`
    align-items: center;
    color: #333;
    display: flex;
    font-size: 16px;
    font-weight: 500;
    text-decoration: none;

    &:hover {
        text-decoration: underline;
    }

    ${separatorTopbar}
`;

const LogoHeader = styled(Logo)`
    ${separatorTopbar}
`;

const LogoSVGStyled = styled(LogoSvg)`
    path {
        fill: ${props => props.theme.componentsTheme.logoERC721Color};
    }
`;

const WalletDropdown = styled(WalletConnectionContentContainer)`
    display: none;
    @media (min-width: ${themeBreakPoints.sm}) {
        align-items: center;
        display: flex;
        ${separatorTopbar}
    }
`;

const ToolbarContent = (props: Props) => {
    const handleLogoClick: React.EventHandler<React.MouseEvent> = e => {
        e.preventDefault();
        props.onGoToHome();
    };

    const comment = (<div dangerouslySetInnerHTML={{ __html: '<!-- comment text -->' }} />);
    
    const startContent = (
        <LogoHeader
            image={<LogoSVGStyled />}
            onClick={handleLogoClick}
            text="Decentracraft"
            textColor={props.theme.componentsTheme.logoERC721TextColor}
        />
    );

    const handleMyWalletClick: React.EventHandler<React.MouseEvent> = e => {
        e.preventDefault();
        props.goToWholesale();
    };
    const handleWholesaleClick: React.EventHandler<React.MouseEvent> = e => {
        e.preventDefault();
        props.goToWholesale();
    };
    const endContent = (
        <>
            <MyWalletLink href="/#/wholesale/my-wholesales" onClick={handleWholesaleClick}>
                My Packages
            </MyWalletLink>
            <WalletDropdown />
            <NotificationsDropdownContainer />
        </>
    );
    const centerContent = <CollectiblesSearch theme={props.theme} />;

    return(
        <div >
        <div dangerouslySetInnerHTML={{ __html: '<!-- Icon used from https://www.freepik.com/free-vector/abstract-polygonal-cyber-sphere_1534720.htm#page=1&query=hud&position=20 -->' }}/>
        <ToolbarContainer startContent={startContent} centerContent={centerContent} endContent={endContent} /> 
        </div>
        );
};

const mapDispatchToProps = (dispatch: any): DispatchProps => {
    return {
        onGoToHome: () => dispatch(goToHome()),
        goToWholesale: () => dispatch(goToWholesale()),
    };
};

const ToolbarContentContainer = withTheme(
    connect(
        null,
        mapDispatchToProps,
    )(ToolbarContent),
);

export { ToolbarContent, ToolbarContentContainer };
