import React from 'react';
import { Route, Switch } from 'react-router';
import styled, { ThemeProvider } from 'styled-components';

import { WHOLESALE_APP_BASE_PATH } from '../../common/constants';
import { getThemeByMarketplace } from '../../themes/theme_meta_data_utils';
import { MARKETPLACES } from '../../util/types';
import { AdBlockDetector } from '../common/adblock_detector';
import { CheckMetamaskStateModalContainer } from '../common/check_metamask_state_modal_container';
import { GeneralLayout } from '../general_layout';

import { CollectibleSellModal } from './collectibles/wholesale_sell_modal';
import { ToolbarContentContainer } from './common/toolbar_content';
import { AllCollectibles } from './pages/all_wholesale';
import { IndividualCollectible } from './pages/individual_wholesale';
import { ListCollectibles } from './pages/list_wholesale';
import { MyCollectibles } from './pages/my_wholesales';

const toolbar = <ToolbarContentContainer />;

const GeneralLayoutERC721 = styled(GeneralLayout)`
    background-color: ${props => props.theme.componentsTheme.backgroundERC721};
`;

export const WholesaleApp = () => {
    console.log('WholesaleApp');
    const themeColor = getThemeByMarketplace(MARKETPLACES.ERC721);
    return (
        <ThemeProvider theme={themeColor}>
            <GeneralLayoutERC721 toolbar={toolbar}>
                <AdBlockDetector />
                <CollectibleSellModal />
                <CheckMetamaskStateModalContainer />
                <Switch>
                    <Route exact={true} path={`${WHOLESALE_APP_BASE_PATH}/`} component={AllCollectibles} />
                    <Route exact={true} path={`${WHOLESALE_APP_BASE_PATH}/my-wholesales`} component={MyCollectibles} />
                    <Route
                        exact={true}
                        path={`${WHOLESALE_APP_BASE_PATH}/list-wholesale`}
                        component={ListCollectibles}
                    />
                    <Route path={`${WHOLESALE_APP_BASE_PATH}/collectible/:id`}>
                        {({ match }) => match && <IndividualCollectible collectibleId={match.params.id} type="collectible"/>}
                    </Route>
                    <Route path={`${WHOLESALE_APP_BASE_PATH}/package/:id`}>
                        {({ match }) => match && <IndividualCollectible collectibleId={match.params.id} type="package"/>}
                    </Route>
                    <Route path={`${WHOLESALE_APP_BASE_PATH}/userpackage/:id`}>
                        {({ match }) => match && <IndividualCollectible collectibleId={match.params.id} type="userpackage" />}
                    </Route>
                </Switch>
            </GeneralLayoutERC721>
        </ThemeProvider>
    );
};
