import queryString from 'query-string';
import React from 'react';
import { connect } from 'react-redux';
import styled, { css } from 'styled-components';

import { setCollectiblesListFilterType, setCollectiblesListSortType } from '../../../store/actions';
import {
    getAllWholesalesFetchStatus,
    getRouterLocationSearch,
    getAllWholesalesForSale,
    getUserWholesales,
} from '../../../store/selectors';
import { themeBreakPoints } from '../../../themes/commons';
import { CollectibleFilterType } from '../../../util/filterable_collectibles';
import { CollectibleSortType } from '../../../util/sortable_collectibles';
import { AllResourcePackagesFetchStatus, ResourcesPackage, StoreState } from '../../../util/types';
import { CenteredWrapper } from '../../common/centered_wrapper';
import { SellCollectiblesButton } from '../marketplace/sell_collectibles_button';

import { CollectiblesCardList } from './wholesale_card_list';
import { CollectiblesListFilter } from './wholesale_list_filter';
import { CollectiblesListSort } from './wholesale_list_sort';

interface OwnProps {
    title: string;
}

interface StateProps {
    resourcePackages: { [key: string]: ResourcesPackage };
    search: string;
    fetchStatus: AllResourcePackagesFetchStatus;
}

interface DispatchProps {
    setSortType: (sortType: CollectibleSortType | null) => any;
    setFilterType: (filterType: CollectibleFilterType | null) => any;
}

type Props = OwnProps & StateProps & DispatchProps;

const FiltersMenu = styled.div`
    display: flex;
    flex-direction: column;
    margin: 0 auto 22px;
    max-width: ${themeBreakPoints.xxl};
    position: relative;
    width: 100%;
    z-index: 1;

    @media (min-width: ${themeBreakPoints.md}) {
        align-items: center;
        flex-direction: row;
        padding-top: 24px;
    }
`;

const CollectiblesFilterDropdown = css`
    margin-bottom: 25px;
    margin-right: auto;
    position: relative;

    @media (min-width: ${themeBreakPoints.md}) {
        margin-bottom: 0;
        margin-right: 25px;
    }

    &:last-child {
        margin-bottom: 0;

        @media (min-width: ${themeBreakPoints.md}) {
            margin-right: 0;
        }
    }
`;

const CollectiblesListSortStyled = styled(CollectiblesListSort)`
    ${CollectiblesFilterDropdown}

    z-index: 5;
`;

const CollectiblesListFilterStyled = styled(CollectiblesListFilter)`
    ${CollectiblesFilterDropdown}

    z-index: 1;
`;

const Title = styled.h1`
    color: ${props => props.theme.componentsTheme.textColorCommon};
    font-size: 18px;
    font-weight: 600;
    line-height: 1.2;
    margin: 0 0 25px;

    @media (min-width: ${themeBreakPoints.md}) {
        margin-bottom: 0;
        margin-right: 30px;
    }
`;

export class CollectiblesList extends React.Component<Props, {}> {
    public componentWillUnmount = () => {
        this.props.setSortType(null);
        this.props.setFilterType(null);
    };

    public render = () => {
        const { title, search, fetchStatus } = this.props;
        const collectibles = Object.keys(this.props.resourcePackages).map(key => this.props.resourcePackages[key]);
        const { sortType, filterType } = this._getSortTypeAndFilterTypeFromLocationSearch(search);
        const isLoading = fetchStatus !== AllResourcePackagesFetchStatus.Success;

        return (
            <CenteredWrapper>
                <FiltersMenu>
                    <Title>{title}</Title>
                    <CollectiblesListSortStyled currentValue={sortType} onChange={this._onChangeSortType} />
                    <CollectiblesListFilterStyled currentValue={filterType} onChange={this._onChangeFilterType} />
                </FiltersMenu>
                <CollectiblesCardList
                    collectibles={collectibles}
                    filterType={filterType}
                    isLoading={isLoading}
                    sortType={sortType}
                />
            </CenteredWrapper>
        );
    };

    private readonly _onChangeSortType = (evt: React.ChangeEvent<HTMLInputElement>) => {
        this.props.setSortType(evt.target.value as CollectibleSortType);
    };

    private readonly _onChangeFilterType = (evt: React.ChangeEvent<HTMLInputElement>) => {
        this.props.setFilterType(evt.target.value as CollectibleFilterType);
    };

    private readonly _getSortTypeAndFilterTypeFromLocationSearch = (search: string) => {
        const parsedSearch = queryString.parse(search);
        return {
            sortType: (parsedSearch.sort as CollectibleSortType) || CollectibleSortType.NewestAdded,
            filterType: (parsedSearch.filter as CollectibleFilterType) || CollectibleFilterType.ShowAll,
        };
    };
}

// "All Collectibles" and "My Collectibles" get different selectors
const allMapStateToProps = (state: StoreState): StateProps => {
    console.log("allMapStateToProps");
    return {
        resourcePackages: getAllWholesalesForSale(state),
        search: getRouterLocationSearch(state),
        fetchStatus: getAllWholesalesFetchStatus(state),
    };
};

const myMapStateToProps = (state: StoreState): StateProps => {
    console.log("myMapStateToProps");
    return {
        resourcePackages: getUserWholesales(state),
        search: getRouterLocationSearch(state),
        fetchStatus: getAllWholesalesFetchStatus(state),
    };
};

const mapDispatchToProps = (dispatch: any): DispatchProps => {
    return {
        setSortType: (sortType: CollectibleSortType | null) => {
            dispatch(setCollectiblesListSortType(sortType));
        },
        setFilterType: (filterType: CollectibleFilterType | null) => {
            dispatch(setCollectiblesListFilterType(filterType));
        },
    };
};

export const AllWholesalesListContainer = connect(
    allMapStateToProps,
    mapDispatchToProps,
)(CollectiblesList);

export const MyWholesalesListContainer = connect(
    myMapStateToProps,
    mapDispatchToProps,
)(CollectiblesList);
