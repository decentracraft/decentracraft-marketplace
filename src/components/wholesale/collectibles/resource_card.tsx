import { BigNumber } from '0x.js';
import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import { WHOLESALE_APP_BASE_PATH, ZERO_ADDRESS } from '../../../common/constants';
import { getEthAccount } from '../../../store/selectors';
import { themeDimensions, themeFeatures } from '../../../themes/commons';
import { Resource, StoreState } from '../../../util/types';

import { OwnerBadge } from './owner_badge';
import { PriceBadge } from './price_badge';

const CollectibleCardWrapper = styled.div`
    display: flex;
    justify-content: space-between;
    margin: 0 0 20px;
    background: ${props => props.theme.componentsTheme.cardBackgroundColor};
    border-radius: ${themeDimensions.borderRadius};
    border: 1px solid ${props => props.theme.componentsTheme.cardBorderColor};
    box-sizing: border-box;
    cursor: pointer;
    overflow: hidden;
    position: relative;
    transition: box-shadow 0.15s linear;
    text-decoration: none;

    &:hover {
        box-shadow: ${themeFeatures.boxShadow};
    }
`;

const ResourceDetailsWrapper = styled.div`
    width: 80%
`;

const ImageWrapper = styled.div<{ color: string; image: string }>`
    background-clip: padding-box;
    background-color: ${props => props.color || props.theme.componentsTheme.cardImageBackgroundColor};
    background-image: url('${props => props.image}');
    background-size: contain;
    height: 90px;
    width: 90px;
    margin: 10px;
`;

const Title = styled.h2`
    color: ${props => props.theme.componentsTheme.cardTitleColor};
    font-size: 14px;
    font-weight: 500;
    line-height: 1.2;
    margin: 0;
    overflow: hidden;
    padding: 10px 0px;
    text-overflow: ellipsis;
    white-space: nowrap;
`;

export const CollectibleDescriptionInnerTitle = styled.h4`
    color: ${props => props.theme.componentsTheme.cardTitleColor};
    font-size: 14px;
    font-weight: 500;
    line-height: 1.2;
    margin: 0 0 10px;
`;

const CollectibleDescriptionText = styled.div`
    color: ${props => props.theme.componentsTheme.cardTitleColor};
    font-feature-settings: 'calt' 0;
    font-size: 14px;
    line-height: 1.6;
    margin: 0 0 20px;

    &:last-child {
        margin: 0;
    }
`;

const ItemAttributes = styled.div`
    position: relative;
    display: inline-grid;
    background-color: rgb(237, 251, 255);
    z-index: 1;
    box-sizing: border-box;
    text-align: center;
    opacity: 0.87;
    border-width: 1px;
    border-style: solid;
    border-color: rgb(45, 156, 219);
    border-image: initial;
    border-radius: 10px;
    padding: 4px;
    margin: 5px 0px 5px 5px;
`;

interface OwnProps {
    resource: Resource;
    // price: BigNumber | null;
    // onClick?: (e: any) => void;
}

interface StateProps {
    ethAccount: string;
}

type Props = StateProps & OwnProps;

class ResourceCard extends React.Component<Props> {
    state = {
        uridata: null,
      }

    componentDidMount() {
        console.log("componentDidMount");
        const { resource, ethAccount, ...restProps } = this.props;
        const { uri, id, supply } = resource;


        fetch(uri)
            .then(res => res.json())
            .then((uridata) => {
            this.setState({ uridata: uridata })
            })
    }

    public render = () => {
        const { resource, ethAccount, ...restProps } = this.props;
        const { uri, id, supply } = resource;

        const { uridata } = this.state;
        
        console.log(resource);

        let itemname = "";
        let itemicon = "";
        let description = "";
        if(uridata != null){
            console.log(uridata);
            // @ts-ignore
            itemname = uridata.name;
            // @ts-ignore
            itemicon = uridata.image;
            // @ts-ignore
            description = uridata.description;
        }
        // let itemicon = uridata.image

        return (
            <CollectibleCardWrapper
                {...restProps}
                id={id}
            >
                <ImageWrapper color={"#ffffff00"} image={itemicon}>
                </ImageWrapper>
                <ResourceDetailsWrapper>
                    <Title>{itemname}</Title>
                    <CollectibleDescriptionText>{description}</CollectibleDescriptionText>
                    <CollectibleDescriptionText>Supply <ItemAttributes>{supply}</ItemAttributes></CollectibleDescriptionText>
                </ResourceDetailsWrapper>
            </CollectibleCardWrapper>
        );
    };
}

const mapStateToProps = (state: StoreState): StateProps => {
    return {
        ethAccount: getEthAccount(state),
    };
};

const ResourceCardContainer = connect(
    mapStateToProps,
    {},
)(ResourceCard);

export { ResourceCard, ResourceCardContainer };
